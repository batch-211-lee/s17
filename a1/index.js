console.log("Hello World");
/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

	function displayPersonalInfo(){
		let yourName = prompt("What is your name?");
		let yourAge = prompt("What is your age?");
		let yourAddress = prompt("Where do you Live?");
		console.log("Hello, " + yourName);
		console.log("Your age is " + yourAge);
		console.log("you live in " + yourAddress);
		alert("Thank you for your input!");
	};
	displayPersonalInfo();

/*

	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

	function displayFavBands(){
		console.log("1. Of Monsters and Men");
		console.log("2. Lord Huron");
		console.log("3. Lumineers");
		console.log("4. Jimi Hendrix");
		console.log("5. Blink 182");
	};

	displayFavBands();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
	function displayFavMovies(){
		const movieScore = "Rotten Tomatoes Ratings: "
		console.log("1. John Carpenter's The Thing");
		console.log(movieScore + "83%");
		console.log("2. Creepshow");
		console.log(movieScore + "65%");
		console.log("3. Terminator 2: Judgement Day");
		console.log(movieScore + "93%");
		console.log("4. Kwaidan");
		console.log(movieScore + "91%");
		console.log("5. The Princess Bride");
		console.log(movieScore + "97%");
	};

	displayFavMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

//printUsers();
let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printFriends();

//console.log(friend1);
//console.log(friend2);