printName();

console.log("Hello World");
//Functions
	//Functions in Javascript are lines/blocks of code that tell our device/ application to perform a certain task when called/invoked
	//Functions are mostly created to create complicated tastks to run several lines of code in succession
	//They are also used to prevent repeating line/blocks of codes that perform the same task/function

	//Function Declarations
		//(function statement) defines a function with the specified parameters

		/*
			Syntax:

			function functionName() {
				code block (statement)
			}


		*/

		//function keyword - used to define JavaScript function
		//functionName - the function name. Functions are named to be able to use later in the code.
		//Function Block ({}) - the statement which comprise the body of the function. This is where the code is executed.
		//we can assign a variable to hold a function, will be explained later.

		function printName(){
			console.log("My name is John.");
		};

		printName();

		//semicolons are used to seperate executable JavaScript statements.

		//Function Invocation
			//the code block and statements inside a function is not immediately executed when the function is defined. The code block and stetements inside a function is executed when the function is invoked or called.
			//it is common to use the term "to call a function" instead of "invoke a function"

			printName();

			declaredFunction(); //error, since it was not defined. It can be hoisted as long as the function have been defined


	//Function Declarations vs Expressions

		//Function Declaration
			//a function can be created through function declaration by using the function keyword and adding a function name
			//declared functions are not executed immediately. They are saved for later use, and will be executed later, when they are invoked/called upon.

			function declaredFunction() {
				console.log("Hello world from declaredFunction()");
			};

			//Note: Hoisting is JS's behavior for certain variable and functions to run or use them before their declaration

		//Function Expression
			//a function can also be stored inside a variable. This is called a function expression
			// a function expression is an anonymous function is assigned to the variableFunction

			//Anonymous function - a function without a name

			//variableFunction(); //error

			/*
			error - function expression, being stored in a let or const variable cannot be hoisted
			*/

			let variableFunction = function(){
				console.log("Hello Again");
			};

			variableFunction();

			//we can also create a function expression of a named function, to invoke the function expression, we invoke it by its variable name and not it's function name
			//function expression are always called using variable name

			let funcExpression = function funcName() {
				console.log("Hello from the other side");
			};

			//funcName(); //error
			funcExpression();

			//you can reassign declared function and function expression to new anonymous functions

			declaredFunction = function() {
				console.log("updated declaredFunction");
			};
			declaredFunction();

			funcExpression = function(){
				console.log("updated funcExpression");
			};

			funcExpression();

			const constantFunc = function(){
				console.log("initialized with const!");
			};
			constantFunc();

			//We cannot reassign a function expression initialized with const.

			/*constantFunc = function(){
				console.log("Cannot be reassigned");
			};
			constantFunc(); //error
			*/

	//Function Scoping

	/*
		Scope is the accesibility/visibility of variables within our program
		JS variables has three types of scope:
		1. Local/block scope
		2. Global scope
		3. Function scope
	*/

	//let globalVar = "Mr. Worldwide";
	//console.log(globalVar);

	{
		let localVar = "Armando Perez";
		console.log(localVar);
		//console.log(globalVar);//error
		//we cannot invoke a global var inside a block if it is not declared before our code block
	}

	let globalVar = "Mr. Worldwide";
	console.log(globalVar);
	//console.log(localVar) //error
	//localVar being in a block, cannot be accessed outside of its code block

	//Function Scope

	/*
		JS has a function scope: each function creates a new scope
		Variables defiend inside our function are not accessible/visible from outside the function
		variable declared with var, let, const are quite similar when declared in a function
	*/

	function showNames(){

		var functionVar = "joe";
		const functionConst = "John";
		let functionLet = "jane";
		console.log(functionVar);
		console.log(functionConst);
		console.log(functionLet);
	};

	showNames();

	/*
		the variables, functionVar, functionConst, functionLet, are function scoped cannot be invoked
	*/


	//Nested Functions

		//You can create another function inside another function
		//This is called a nested function

		function myNewFunction(){
			let name = "Cee";

			function nestedFunction(){
				let nestedName = "Thor";
				console.log(name);
				console.log(nestedName)
			}
			//console.log(nestedName);//error not defined inside this block
			nestedFunction();
		};
		myNewFunction();
		//nestedFunction();
		/*
			since this function is declared inside myNewFunction, it too cannot be invoked outside of the function it was declared in
		*/

	//Function Global Scoped Variable

	let globalName = "Nej";

	function myNewFunction2(){
		let nameInside = "Martin";
		//Variables declared globally or outside any function that has global scope
		//Global variables can be accessed anywhere in a JS program including from inside our function
		console.log(globalName);
	}

	myNewFunction2();

//Using alert()

	//alert allows us to show a small window at the top of our browser page, to show information to our users
	//as opposed to our usual console.log() which will only show the message on the console
	//it allows us to show us a short dialog or instruction to our user.
	//this page will wait until the user dismisses the dialog

	alert("Hellow World"); //this will run immediately when the page loads

	//alert() syntax
	//alert("<messageInString>")
	//you can do it in numbers too

	//you can aslo use alert() to show the user from a later function invoked

	function showSampleAlert(){
		alert("Hello User");
	};
	showSampleAlert();

	console.log("I will only log in the console when the alert is dismissed");

	//notes on alert()
		//show only alert() for short messages
		//do not overuse the alrt() because programmers/JS will have to wait

	//Using prompt ()

		//prompt() allows us to show a small window at the top of our browser to gather user input. much like alert() the page will wait for the untile the user completes their input
		//the input will be returned as a string once the user dismisses the window

		let samplePrompt = prompt("Enter your name.");
		console.log("Hello, " + samplePrompt);

		/*
			prompt () syntax:
			prompt("<dialogInString>");
		*/

		let sampleNullPrompt = prompt("don't enter anything");
		console.log(sampleNullPrompt);
		//prompt() returns an empty string when there is no input, and null if the user cancels the prompt()

		//prompt() can be used for us to gather user input and used in our code
		//however since prompt() will have the page wait until the user dismisses the window, it must not be over used

		//prompt() used globally it will run immediately, so for better user experience it is much better to use them accordingly or add them inside a function

		function printWelcomeMessage(){
			let firstName = prompt("Enetr your first name");
			let lastName = prompt("Enter your last name")
			console.log("hello, " + firstName + " " + lastName + "!")
			console.log("Welcome to my page!");
		};

		printWelcomeMessage();

		//Function Naming Conventions

		//function names should be definitive or the task it will perform. it usually contains a verb

		function getCourses(){
			let courses = ["Science", "Math", "English"];
			console.log(courses);
		}

		getCourses();

		//avoid generic names to avoid confusion within your code

		function get(){
			let name = "Jamie";
			console.log(name);
		};

		get();

		//avoid pointless or inappropriate names

		function pikachu(){
			console.log(25%5);
		};

		pikachu();

		//name your functions in smallcaps and follow camel case when naming variables and functions

		function displayCarInfo(){
			console.log("Brand: Toyota");
			console.log("Sedan");
			console.log("Price: 1,500,000")
		};

		displayCarInfo();